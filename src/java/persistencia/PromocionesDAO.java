package persistencia;

import entidades.Promocion;
import java.util.ArrayList;

public class PromocionesDAO {

    public static ArrayList obtener() {
        String promo = "Cabañas, descuento del 50%, fin de semana.";
        ArrayList miListado = new ArrayList();
        
        Promocion miPromo = new Promocion("Cabañas","50 de descuento");
        Promocion miPromo2 = new Promocion("Empanadas","30 de descuento");
        Promocion miPromo3 = new Promocion("Donas","3x2");
        Promocion miPromo4 = new Promocion("Jeans Falabella","2x1");

        miListado.add(miPromo);
        miListado.add(miPromo2);
        miListado.add(miPromo3);
        miListado.add(miPromo4);
        
        
        return miListado;
    }
    
}
