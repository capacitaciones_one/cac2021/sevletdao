package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import persistencia.PromocionesDAO;


@WebServlet(name="Promociones", urlPatterns={"/Promociones"})
public class Promociones extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Llamaste al servidor, estas en el metodo doGet()"); 
//        String promocionDelDia = "2x1 en cine Hoyts";
//        resp.getWriter().println(promocionDelDia);
        ArrayList resultado = PromocionesDAO.obtener();
        resp.getWriter().println(resultado);


    }
   
 

}
